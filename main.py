from flask import Flask, Response
from prometheus_client import CollectorRegistry, Gauge, generate_latest
import requests

app = Flask(__name__)
registry = CollectorRegistry()

# JSON data endpoint
json_data_url = "https://api.example.com/data.json"

# Function to fetch JSON data from the endpoint
def fetch_json_data():
    response = requests.get(json_data_url)
    if response.status_code == 200:
        return response.json()
    else:
        return None

# Endpoint to expose Prometheus metrics
@app.route('/metrics')
def metrics():
    data = fetch_json_data()
    if data:
        # Create Prometheus gauge metrics for temperature and humidity
        temperature_gauge = Gauge("temperature", "Temperature in degrees Celsius", registry=registry)
        humidity_gauge = Gauge("humidity", "Relative humidity in percentage", registry=registry)

        # Set the metric values from the fetched JSON data
        temperature_gauge.set(data["temperature"])
        humidity_gauge.set(data["humidity"])

    return Response(generate_latest(registry), mimetype='text/plain')

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8000)